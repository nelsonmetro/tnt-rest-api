<?php
use Phalcon\Http\Response;

$app->post("/greatreset", function () use ($app) {
  $bot = $app->request->getJsonRawBody();
  $var = $app->getSharedService("db");

  $statement = $var->prepare("CALL GREATRESET()");
  return respond($statement->execute());
});

function respond($pdo)
{
  $response = new Response();
  if ($pdo) {
    $response->setStatusCode(201, "Success");
    $response->setJsonContent([
      "status" => "OK",
      "data" => $pdo,
    ]);
  } else {
    $response->setStatusCode(409, "Conflict");

    /* $errors = []; */
    /* foreach ($status->getMessages() as $message) { */
    /*   $errors[] = $message->getMessage(); */
    /* } */

    $response->setJsonContent([
      "status" => "ERROR",
      /* "messages" => $errors, */
    ]);
  }
  return $response;
}

?>
