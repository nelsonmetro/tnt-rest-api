<?php
use Root\Models\Participant;

$app->get("/participants/{text}", function ($text)  {
  $quest = Participant::find(["conditions" => "FIRST_NAME LIKE :txt: OR LAST_NAME LIKE :txt:", "bind" => ["txt" => "%$text%"]]);
  return json_encode($quest->toArray());
});

$app->get("/participants", function () {
  $quest = Participant::find();
  return json_encode($quest->toArray());
});

$app->get("/participant/{id}", function ($id)  {
  $quest = Participant::find(["conditions" => "ID = :id:", "bind" => ["id" => $id]]);
  return json_encode($quest->toArray());
});

$app->get("/student/{id:[0-9]+}", function ($id)  {
  $quest = Participant::find(["conditions" => "STUDENT_NO = :id:", "bind" => ["id" => $id]]);
  return json_encode($quest->toArray());
});

$app->post("/participant/add", function () use ($app) {
  // curl -i -X POST -d '{"fname":"Evidence", "lname":"Bangene", "title":"Mr", "email":"super@mail.com", "volun":true, "nocon":true, "langu":true, "nocos":true, "locat":"[4415,-957]"}' \
  // 10.0.0.10/participant/new

  $arg = $app->request->getJsonRawBody();
  $pdo = $app->getSharedService("db");

  $stmt = $pdo->prepare(
    "CALL NEWPARTICIPANT(:fname, :lname, :title, :email, :volun, :nocon, :langu, :nocos, :locat)"
  );

  $success = $stmt->execute((array) $arg);
  $stmt->closeCursor();

  return respond($success);
});

?>
