<?php
use Root\Models\Objective;
use Root\Models\Quest;

$app->get("/objectives", function () {
  $objectives = Objective::find(["order" => "ID"]);
  return json_encode($objectives->toArray());
});

$app->get("/quest/{id:[0-9]+}", function ($id) {
  $quest = Quest::find(["conditions" => "ID = :id:", "bind" => ["id" => $id]]);
  return json_encode($quest->toArray());
});
?>
