<?php
namespace Root\Models;
use Phalcon\Mvc\Model;

/**
 * Consent
 */
class Consent extends Model
{
  public $id;
  public $date;
  public $voluntary;
  public $no_consequence;
  public $language;
  public $no_cost;
  public $location;

  /**
   * undocumented function
   *
   * @return void
   */
  public function initialize()
  {
    $this->setSource("consent");
    $this->belongsTo("id", Participant::class, "id");
  }
}
?>
