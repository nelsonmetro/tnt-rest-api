<?php

namespace Root\Models;
use Phalcon\Mvc\Model;

/**
* Class Objective
* @author Gowan Cephus
*/
class Objective extends Model
{
  public $id;
  public $description;

  function initialize()
  {
    $this->setSource("objective");
  }
}
