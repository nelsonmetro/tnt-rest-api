<?php
namespace Root\Models;
use Phalcon\Mvc\Model;

/**
 * Participant
 */
class Participant extends Model
{
  public $id;
  public $title;
  public $first_name;
  public $last_name;
  public $student_no;
  public $email;

  /**
   * undocumented function
   *
   * @return void
   */
  public function initialize()
  {
    $this->setSource("participant");
    $this->hasOne("id", Consent::class, "id");
  }
}
?>
