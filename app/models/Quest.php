<?php
namespace Root\Models;
use Phalcon\Mvc\Model;

/**
 * Class Quest
 * @author Gowan Cephus
 */
class Quest extends Model
{
  public $id;
  public $description;
  public $tale;
  public $q;
  public $question;
  public $type;
  public $a;
  public $answer;

  function initialize()
  {
    $this->setSource("QUEST");
  }
}
