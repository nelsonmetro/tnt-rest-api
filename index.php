<?php

header("Access-Control-Allow-Origin: http://127.0.0.1:8080");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH, OPTIONS, HEAD");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type");

use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;

$loader = new Loader();
$loader->registerNamespaces([
  "Root\Models" => __DIR__ . "/app/models/",
]);
$loader->register();

$container = new FactoryDefault();
$container->set("db", function () {
// retrieve connection string from INI file
$conexion = parse_ini_file("sql/connection.ini");
  return new PdoMysql([
    "host" => $conexion["host"],
    "username" => $conexion["user"],
     'password' => $conexion['password'],
    "dbname" => $conexion["dbname"],
  ]);
});

$app = new Micro($container);

require_once "app/endpoints/global.php";
require_once "app/endpoints/participant.php";
require_once "app/endpoints/question.php";

$app->handle($_SERVER["REQUEST_URI"]);
