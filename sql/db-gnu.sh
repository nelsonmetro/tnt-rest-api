#!/bin/bash

read -p 'Is your MySQL root user password protected?[y/N]: ' haspass
haspass=`echo $haspass | tr '[A-Z]' '[a-z]'` #transform value to lower case

while [ $haspass != "y" ] && [ $haspass != "n" ]; do
	echo option not understood!
	echo please use Y or N
	echo
	read -p 'Is your MySQL root user password protected?[y/N]: ' haspass
done

if [ "$haspass" = "y" ]; then
	stty -echo
	printf "Enter MySQL Root Password: "
	read  passvar
	echo
	mysql -uroot -p$passvar trackntrace < schema.sql
	mysql -uroot -p$passvar trackntrace < data.sql
	mysql -uroot -p$passvar trackntrace < triggers.sql
else
        mysql -uroot trackntrace < schema.sql
        mysql -uroot trackntrace < data.sql
	mysql -uroot trackntrace < triggers.sql
fi
