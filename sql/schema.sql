-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: trackntrace
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `trackntrace`
--

/*!40000 DROP DATABASE IF EXISTS `trackntrace`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `trackntrace` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `trackntrace`;

--
-- Temporary table structure for view `QUEST`
--

DROP TABLE IF EXISTS `QUEST`;
/*!50001 DROP VIEW IF EXISTS `QUEST`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `QUEST` (
  `ID` tinyint NOT NULL,
  `DESCRIPTION` tinyint NOT NULL,
  `TALE` tinyint NOT NULL,
  `Q` tinyint NOT NULL,
  `QUESTION` tinyint NOT NULL,
  `TYPE` tinyint NOT NULL,
  `A` tinyint NOT NULL,
  `ANSWER` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `ANSWER` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `consent`
--

DROP TABLE IF EXISTS `consent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consent` (
  `ID` char(3) CHARACTER SET utf8 NOT NULL,
  `DATE` datetime NOT NULL DEFAULT current_timestamp(),
  `VOLUNTARY` tinyint(1) NOT NULL,
  `NO_CONSEQUENCE` tinyint(1) NOT NULL,
  `LANGUAGE` tinyint(1) NOT NULL,
  `NO_COST` tinyint(1) NOT NULL,
  `LOCATION` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `consent_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `participant` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `objective`
--

DROP TABLE IF EXISTS `objective`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objective` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(48) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DESCRIPTION` (`DESCRIPTION`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `option`
--

DROP TABLE IF EXISTS `option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option` (
  `QUESTION` tinyint(3) unsigned NOT NULL,
  `ANSWER` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`QUESTION`,`ANSWER`),
  KEY `ANSWER` (`ANSWER`),
  CONSTRAINT `option_ibfk_1` FOREIGN KEY (`QUESTION`) REFERENCES `question` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `option_ibfk_2` FOREIGN KEY (`ANSWER`) REFERENCES `answer` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `participant`
--

DROP TABLE IF EXISTS `participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant` (
  `ID` char(3) CHARACTER SET utf8 NOT NULL,
  `FIRST_NAME` varchar(20) NOT NULL,
  `LAST_NAME` varchar(20) NOT NULL,
  `STUDENT_NO` char(9) CHARACTER SET utf8 DEFAULT NULL,
  `TITLE` enum('Mr','Miss','Prof','Dr','Mrs','Rev') DEFAULT NULL,
  `EMAIL` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `STUDENT_MO` (`STUDENT_NO`),
  UNIQUE KEY `STUDENT_NO` (`STUDENT_NO`),
  UNIQUE KEY `EMAIL` (`EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qtype`
--

DROP TABLE IF EXISTS `qtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qtype` (
  `ID` char(2) CHARACTER SET utf8 NOT NULL,
  `NAME` varchar(20) NOT NULL,
  `DESCRIPTION` varchar(95) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `QUESTION` varchar(255) NOT NULL,
  `TYPE` char(2) CHARACTER SET utf8 NOT NULL,
  `OBJECTIVE` tinyint(3) unsigned NOT NULL,
  `SYNOPSIS` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `QUESTION` (`QUESTION`),
  KEY `TYPE` (`TYPE`),
  KEY `OBJECTIVE` (`OBJECTIVE`),
  KEY `SYNOPSIS` (`SYNOPSIS`),
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`TYPE`) REFERENCES `qtype` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `question_ibfk_2` FOREIGN KEY (`OBJECTIVE`) REFERENCES `objective` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `question_ibfk_3` FOREIGN KEY (`SYNOPSIS`) REFERENCES `synopsis` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `PARTICIPANT` char(3) CHARACTER SET utf8 NOT NULL,
  `QUESTION` tinyint(3) unsigned NOT NULL,
  `MODIFIED` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`PARTICIPANT`,`QUESTION`),
  KEY `QUESTION` (`QUESTION`),
  CONSTRAINT `response_ibfk_1` FOREIGN KEY (`PARTICIPANT`) REFERENCES `participant` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `response_ibfk_2` FOREIGN KEY (`QUESTION`) REFERENCES `question` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sheet`
--

DROP TABLE IF EXISTS `sheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sheet` (
  `PARTICIPANT` char(3) CHARACTER SET utf8 NOT NULL,
  `QUESTION` tinyint(3) unsigned NOT NULL,
  `ANSWER` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`PARTICIPANT`,`QUESTION`,`ANSWER`),
  KEY `QUESTION` (`QUESTION`,`ANSWER`),
  CONSTRAINT `sheet_ibfk_1` FOREIGN KEY (`PARTICIPANT`, `QUESTION`) REFERENCES `response` (`PARTICIPANT`, `QUESTION`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sheet_ibfk_2` FOREIGN KEY (`QUESTION`, `ANSWER`) REFERENCES `option` (`QUESTION`, `ANSWER`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `synopsis`
--

DROP TABLE IF EXISTS `synopsis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `synopsis` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `TALE` text NOT NULL,
  `OBJECTIVE` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `OBJECTIVE` (`OBJECTIVE`),
  CONSTRAINT `synopsis_ibfk_1` FOREIGN KEY (`OBJECTIVE`) REFERENCES `objective` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'trackntrace'
--
/*!50003 DROP PROCEDURE IF EXISTS `greatreset` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `greatreset`()
BEGIN
	DELETE FROM participant WHERE `STUDENT_NO` IS NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `newparticipant` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `newparticipant`(
	IN fname VARCHAR(20)
	,IN lname VARCHAR(20)
	,IN title VARCHAR(4)
	,IN email VARCHAR(45)
	,IN volun BOOLEAN
	,IN nocon BOOLEAN
	,IN langu BOOLEAN
	,IN nocos BOOLEAN
	,IN locat VARCHAR(90)
	
	
	

)
BEGIN

	
	 
	
	
	

	START TRANSACTION;
		INSERT INTO participant(TITLE, FIRST_NAME, LAST_NAME, EMAIL) 
		VALUES(title, fname, lname, email);

		SELECT @NEW_PARTICIPANT;

		INSERT INTO consent(ID, VOLUNTARY, NO_CONSEQUENCE, LANGUAGE, NO_COST, LOCATION) 
		VALUES(@NEW_PARTICIPANT, volun, nocon, langu, nocos, locat);
	COMMIT;

	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `trackntrace`
--

USE `trackntrace`;

--
-- Final view structure for view `QUEST`
--

/*!50001 DROP TABLE IF EXISTS `QUEST`*/;
/*!50001 DROP VIEW IF EXISTS `QUEST`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `QUEST` AS select `o`.`ID` AS `ID`,`o`.`DESCRIPTION` AS `DESCRIPTION`,`s`.`TALE` AS `TALE`,`q`.`ID` AS `Q`,`q`.`QUESTION` AS `QUESTION`,`q`.`TYPE` AS `TYPE`,`a`.`ID` AS `A`,`a`.`ANSWER` AS `ANSWER` from ((((`objective` `o` join `synopsis` `s`) join `question` `q`) join `option` `n`) join `answer` `a`) where `o`.`ID` = `s`.`OBJECTIVE` and `s`.`ID` = `q`.`SYNOPSIS` and `n`.`QUESTION` = `q`.`ID` and `a`.`ID` = `n`.`ANSWER` order by `o`.`ID`,`q`.`ID`,`a`.`ID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-03 16:27:14
