@echo off
set /p hasPass=" Do you have MySql root Password?[y/N] "
::set pass=
IF /I "%hasPass%"=="Y" (

	set /p pass=" Enter MySql root Password: "
	ECHO ON
	mysql -uroot -p%pass% trackntrace < schema.sql
	mysql -uroot -p%pass% trackntrace < data.sql
	mysql -uroot -p%pass% trackntrace < triggers.sql

) ELSE (
	ECHO ON
	mysql -uroot trackntrace < schema.sql
	mysql -uroot trackntrace < data.sql
	mysql -uroot trackntrace < triggers.sql
)
