# README #

Clone project to **Document Root** of relevant virtual host

### What is this repository for? ###

* RESTful API built with the Phalcon C externsion
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
Project has the following dependencies

* PHP 7.4 and Phalcon 4 to be installed.
* Apache 2, configured to allow **.htaccess** files
* MariaDB

The **sql/** directory contains **.sql** files to setup the database schema. Load the files to your database server.   
Alternatively: run the shell script relevant to your operating system (**.sh** for GNU, or **.bat** for Windows), in the **sql/** directory.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Daluxolo Msikinya (s215013395@mandela.ac.za)
* School of ICT, Nelson Mandela University
